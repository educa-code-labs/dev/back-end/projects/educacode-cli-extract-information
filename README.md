<div align="center">

![logo](https://i.imgur.com/YUZzmXm.png)

Educacode-CLI
=======================================

[![Licence: MIT](https://img.shields.io/badge/Licence-MIT-green)](LICENCE)
[![Team](https://img.shields.io/badge/Team-General-red)](https://gitlab.com/educa-code-labs/general)

</div>


* * *

O objetivo principal deste repositório é criar um aplicativo de linha de comando que execute várias operações relacionadas ao projeto Educacode. 
O aplicativo inclui diversos comandos que podem ser usados para validar o seu arquivo de configuração, bem como enviá-lo para análise na nossa plataforma.

### Requisitos

Esses são os requisitos para você conseguir executar esse aplicativo:

- PHP >= 8.2

### Instalação

A maneira recomendada de instalar este projeto é seguindo estas etapas:

1. Realize o clone do projeto para a sua máquina

```shell
git clone git@gitlab.com:educa-code-labs/dev/back-end/projects/educacode-cli-php.git
```

Se você estiver usando as configurações de ambiente do [educa-code-labs](https://gitlab.com/educa-code-labs) recomendamos fazer o clone na seguinte pasta `/home/[seu-usuário]/vhost`.

2. Acessar as pastas do projeto

```shell
cd educacode-cli-php
make init
```

### Software stack

Esse projeto roda nos seguintes softwares:

- Git 2.33+
- PHP 8.2
- Laravel Zero
- Gitlab 15.4.0-pre

### Changelog

Por favor, veja [CHANGELOG](CHANGELOG.md) para obter mais informações sobre o que mudou recentemente.

### Seja um dos contribuidores

Quer fazer parte desse projeto? Clique AQUI e leia [como contribuir](CONTRIBUTING.md).

## Segurança

Se você descobrir algum problema relacionado à segurança, envie um e-mail para reinangabriel1520@gmail.com em vez de usar o issue.

### Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE) para mais detalhes.
