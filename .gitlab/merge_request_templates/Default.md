<!-- Before proceeding, please check if you need to apply a specific MR description template from the dropdown menu above next to `Description` (e.g. blog post, website bug report). -->

## Por que essa mudança está sendo feita?

<!--
Forneça uma resposta detalhada à pergunta sobre **por que** essa mudança está sendo proposta.

Exemplo: `Discutimos o assunto no Discord - (cópia da conversa do Discord). O processo atual não é eficiente, esta MR torna a descrição de X mais clara e ajuda a avançar Y.`
-->

## Lista de verificação do autor

<!-- Verifique a lista de verificação e certifique-se de marcá-la antes que o MR seja mesclado. -->

- [ ] Forneceu um título conciso para este [Merge Request (MR)][mr]
- [ ] Adicionada uma descrição a este MR explicando os motivos da mudança proposta, por (**diga por que, não apenas o que**)
    - Copie/cole a conversa do Discord para documentá-la para mais tarde ou faça upload de capturas de tela. Verifique se nenhum dado confidencial foi adicionado.
- [ ] Atribuir revisores para este MR
