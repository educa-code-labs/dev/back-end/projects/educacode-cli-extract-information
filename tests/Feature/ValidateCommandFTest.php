<?php

namespace Tests\Feature;

use Tests\TestCase;

class ValidateCommandFTest extends TestCase
{
    public function testExecutionNoValidParameters(): void
    {
        $this->expectExceptionMessage('Could not find a configuration file in the directory');
        $this->artisan('validate');
    }
}
