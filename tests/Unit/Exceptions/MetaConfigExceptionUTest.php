<?php

namespace Tests\Unit\Exceptions;

use App\Exceptions\MetaConfigException;
use Tests\TestCase;

class MetaConfigExceptionUTest extends TestCase
{
    public function testGetErros()
    {
        $exception = new MetaConfigException(
            message: 'Invalid configuration',
            erros: [
                'name' => 'The name field is required',
            ],
            code: 1,
            previous: null
        );

        $this->assertEquals('The name field is required', $exception->getErros()['name']);
    }
}
