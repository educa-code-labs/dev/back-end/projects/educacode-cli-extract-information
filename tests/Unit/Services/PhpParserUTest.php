<?php

namespace Tests\Unit\Services;

use App\Dto\ConfigDto;
use App\Services\PhpParser;
use Illuminate\Support\Facades\File;
use Mockery;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class PhpParserUTest extends TestCase
{
    private MockObject|ConfigDto $mockDto;

    protected function setUp(): void
    {
        parent::setUp();

        $this->mockDto = $this->createMock(ConfigDto::class);
    }

    /**
     * @dataProvider dataProviderGetExerciseFiles
     */
    public function testGetExerciseFiles(
        string $exercise_path,
        array $files,
        array $expect_data
    ): void {
        $mock = Mockery::mock('alias:'.File::class);
        foreach (['solution', 'test', 'example'] as $type) {
            foreach ($files[$type] as $key => $file) {
                $mock->allows('get')
                    ->with("$exercise_path/$file")
                    ->andReturns($expect_data[$type.'_files'][$key]['raw']);
            }
        }

        $instance = new PhpParser($this->mockDto, 'app');
        $data = $instance->getExerciseFiles($exercise_path, $files);
        $this->assertEquals($expect_data, $data);
    }

    public static function dataProviderGetExerciseFiles(): array
    {
        return [
            'check get exercise files' => [
                'exercise_path' => '/app',
                'files' => [
                    'solution' => [
                        'solution.php',
                        'solution_two.php',
                    ],
                    'test' => [
                        'test.php',
                        'test_two.php',
                    ],
                    'example' => [
                        'example.php',
                        'example_two.php',
                    ],
                ],
                'expect_data' => [
                    'solution_files' => [
                        [
                            'name' => 'solution.php',
                            'raw' => 'This is a content',
                        ],
                        [
                            'name' => 'solution_two.php',
                            'raw' => 'Hello',
                        ],
                    ],
                    'test_files' => [
                        [
                            'name' => 'test.php',
                            'raw' => 'Test file',
                        ],
                        [
                            'name' => 'test_two.php',
                            'raw' => 'Test two file content',
                        ],
                    ],
                    'example_files' => [
                        [
                            'name' => 'example.php',
                            'raw' => 'This is a example',
                        ],
                        [
                            'name' => 'example_two.php',
                            'raw' => 'This is a new example',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider dataProviderGetExerciseConfig
     */
    public function testGetExerciseConfig(string $exercise_path, array $expect_content, ?string $exception): void
    {
        $mock = Mockery::mock('alias:'.File::class);
        $mock->allows('exists')
            ->with($exercise_path)
            ->andReturns(empty($exception));

        if ($exception) {
            $this->expectExceptionMessage($exception);
        }

        if ($expect_content) {
            $mock->allows('json')
                ->with($exercise_path)
                ->andReturns($expect_content);
        }

        $instance = new PhpParser($this->mockDto, 'app');
        $config = $instance->getExerciseConfig($exercise_path);

        $this->assertEquals($expect_content, $config);
    }

    public static function dataProviderGetExerciseConfig(): array
    {
        return [
            'validation with invalid path' => [
                'exercise_path' => '/app',
                'expect_content' => [],
                'exception' => 'Configuration file not found in project',
            ],
            'validation with valid path' => [
                'exercise_path' => '/app',
                'expect_content' => [
                    'blurb' => 'Escrevendo o primeiro `Olá mundo` em PHP',
                    'authors' => [
                        'Reinan',
                    ],
                ],
                'exception' => null,
            ],
        ];
    }
}
