<?php

namespace Tests\Unit\Helpers;

use App\Helpers\DirectoryHelper;
use Tests\TestCase;

class DirectoryHelperUTest extends TestCase
{
    /**
     * @dataProvider dataProviderGetDirectoryByCWD
     */
    public function testGetDirectoryByCWD(?string $dir, string|false $expect_dir): void
    {
        $this->assertEquals($expect_dir, DirectoryHelper::getDirectoryByCWD($dir));
    }

    public static function dataProviderGetDirectoryByCWD(): array
    {
        return [
            'verification by passing the directory as null' => [
                'dir' => null,
                'expect_dir' => getcwd(),
            ],
            'verification by passing the directory as empty' => [
                'dir' => '',
                'expect_dir' => getcwd(),
            ],
            'verification by passing the directory as point' => [
                'dir' => '.',
                'expect_dir' => getcwd(),
            ],
            'verification by passing the invalid directory' => [
                'dir' => '/app-fake',
                'expect_dir' => false,
            ],
            'verification by passing the directory' => [
                'dir' => '/app',
                'expect_dir' => dirname(__DIR__, 3).'/app',
            ],
        ];
    }
}
