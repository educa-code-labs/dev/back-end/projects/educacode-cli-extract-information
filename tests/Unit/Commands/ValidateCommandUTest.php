<?php

namespace Tests\Unit\Commands;

use App\Commands\ValidateCommand;
use App\Factorys\ExerciseParserFactory;
use App\Services\PhpParser;
use Illuminate\Support\Facades\File;
use Mockery;
use Symfony\Component\Console\Input\InputInterface;
use Tests\TestCase;

class ValidateCommandUTest extends TestCase
{
    /**
     * @dataProvider dataProviderConfigFilePath
     */
    public function testGetConfigFilePath(
        ?string $dir,
        string $file_path,
        ?array $config_file = [],
        string|false $exception = false,
    ): void {
        if ($exception) {
            $this->expectExceptionMessage($exception);
        }

        $input_mocked = $this->createMock(InputInterface::class);
        $input_mocked->expects($this->once())
            ->method('getOption')
            ->with('dir')
            ->willReturn($dir);

        if ($config_file) {
            $mocked_directory = Mockery::mock('alias:'.File::class);
            $mocked_directory->allows('isFile')
                ->with($config_file['path'])
                ->andReturns($config_file['return']);
        }

        $command = $this->getValidateCommandInstance();
        $command->setInput($input_mocked);
        $response = $command->getConfigFilePath();

        $this->assertEquals($file_path, $response);
    }

    public static function dataProviderConfigFilePath(): array
    {
        return [
            'invalid directory validation' => [
                'dir' => getcwd().'/var/cache/invalid-dir',
                'result' => '',
                'config_file' => [],
                'exception' => 'The directory you are using is not valid',
            ],
            'current directory config file not exists' => [
                'dir' => null,
                'file_path' => '',
                'config_file' => [
                    'path' => getcwd().'/exercises-config.yml',
                    'return' => false,
                ],
                'exception' => 'Could not find a configuration file in the directory',
            ],
            'current directory config file exists' => [
                'dir' => null,
                'file_path' => getcwd().'/exercises-config.yml',
                'config_file' => [
                    'path' => getcwd().'/exercises-config.yml',
                    'return' => true,
                ],
            ],
        ];
    }

    /**
     * @dataProvider dataProviderValidateConfig
     */
    public function testValidateConfig(
        array $content,
        bool $has_error,
        ?array $validation_data,
        ?string $exception
    ): void {
        $command = $this->getMockBuilder(ValidateCommand::class)
            ->onlyMethods(['error', 'table'])
            ->getMock();

        $command->expects($has_error ? $this->once() : $this->never())
            ->method('error')
            ->with('There was an inconsistency in the validation of the information');

        if ($validation_data) {
            $command->expects($this->once())
                ->method('table')
                ->with(['Field', 'Message'], $validation_data);
        }

        if ($exception) {
            $this->expectExceptionMessage($exception);
        }

        $command->validateConfig($content);
    }

    public static function dataProviderValidateConfig(): array
    {
        return [
            'invalid version' => [
                'content' => [
                    'version' => 'AS',
                ],
                'has_error' => false,
                'validation_data' => null,
                'exception' => 'The version your config file is defending is invalid.',
            ],
            'empty content' => [
                'content' => [],
                'has_error' => true,
                'validation_data' => [
                    ['version', 'The Version is required'],
                    ['config', 'The Config is required'],
                    ['config.language', 'The Config.language is required'],
                    ['config.slug', 'The Config.slug is required'],
                    ['config.blurb', 'The Config.blurb is required'],
                    ['exercises', 'The Exercises is required'],
                    ['exercises.practice', 'The Exercises.practice is required'],
                ],
                'exception' => 'Please correct the information in the config file!!!',
            ],
            'invalid content' => [
                'content' => [
                    'version' => '1',
                    'config' => [
                        'language' => 'Assembly',
                        'slug' => 'b',
                        'blurb' => 'null',
                    ],
                    'exercises' => [
                        'practice' => [
                            'key' => 'value',
                        ],
                    ],
                ],
                'has_error' => true,
                'validation_data' => [
                    ['config.language', 'The Config.language only allows \'PHP\', or \'JAVA\''],
                    ['config.blurb', 'The Config.blurb minimum is 5'],
                    ['exercises.practice.key.slug', 'The Exercises practice key slug is required'],
                    ['exercises.practice.key.name', 'The Exercises practice key name is required'],
                    ['exercises.practice.key.difficulty', 'The Exercises practice key difficulty is required'],
                ],
                'exception' => 'Please correct the information in the config file!!!',
            ],
            'valid content' => [
                'content' => [
                    'version' => '1',
                    'config' => [
                        'language' => 'PHP',
                        'slug' => 'php',
                        'blurb' => 'Delegadis gente finis, bibendum egestas augue arcu ut est',
                    ],
                    'exercises' => [
                        'practice' => [
                            [
                                'slug' => 'level-1',
                                'name' => 'hello',
                                'difficulty' => 1,
                            ],
                        ],
                    ],
                ],
                'has_error' => false,
                'validation_data' => null,
                'exception' => null,
            ],
        ];
    }

    /**
     * @dataProvider dataProviderHandle
     */
    public function testHandle(string $file_path, ?array $content, ?string $exception): void
    {
        $mocked_command = $this->getMockBuilder(ValidateCommand::class)
            ->onlyMethods([
                'getConfigFilePath',
                'info',
                'warn',
                'error',
                'validateConfig',
            ])
            ->getMock();

        $mocked_command->expects($this->once())
            ->method('getConfigFilePath')
            ->willReturn($file_path);

        if ($content) {
            $mocked_file = Mockery::mock('alias:'.File::class);
            $mocked_file->allows('get')
                ->with($file_path)
                ->andReturns(yaml_emit($content));
        }

        if (! $exception) {
            $mocked_parse = Mockery::mock('alias:'.ExerciseParserFactory::class);
            $mocked_parse->allows('build')
                ->withAnyArgs()
                ->andReturn(
                    $this->createMock(PhpParser::class)
                );
        }

        if ($exception) {
            $this->expectExceptionMessage($exception);
        }

        $mocked_command->handle();
    }

    public static function dataProviderHandle(): array
    {
        return [
            'processing with valid information' => [
                'file_path' => 'application/exercises-config.yml',
                'content' => [
                    'version' => '1',
                    'config' => [
                        'language' => 'PHP',
                        'slug' => 'php',
                        'blurb' => 'Delegadis gente finis, bibendum egestas augue arcu ut est',
                    ],
                    'exercises' => [
                        'practice' => [
                            [
                                'slug' => 'level-1',
                                'name' => 'hello',
                                'difficulty' => 1,
                            ],
                        ],
                    ],
                ],
                'exception' => null,
            ],
        ];
    }

    private function getValidateCommandInstance(): ValidateCommand
    {
        return new ValidateCommand();
    }
}
