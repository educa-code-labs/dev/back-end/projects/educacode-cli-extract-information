FROM registry.gitlab.com/educa-code-labs/infra/educacode-php-runtime/php-dev-8.2-cli:latest AS builder
USER root
WORKDIR "/application"

ARG VERSION=develop

COPY . /application
RUN chown -R www-data:www-data /application

RUN composer install
RUN php educacode app:build --build-version=1.0.0

FROM alpine:3.18.0 AS app

RUN apk add --no-cache ca-certificates git make
RUN apk update && apk upgrade
RUN apk add php composer php81-pecl-yaml php81-fileinfo php81-session php81-tokenizer

WORKDIR /root/

COPY --from=builder /application/builds/educacode /bin
RUN chmod +x /bin/educacode

CMD ["educacode"]
