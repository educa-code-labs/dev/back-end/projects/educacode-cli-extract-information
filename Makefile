init:
	echo "Start"
up:
	docker compose up
down:
	docker compose down -v
php:
	docker exec -it educacode-cli-extract-information bash
app:
	php application
style:
	./vendor/bin/pint --test
static:
	./vendor/bin/psalm --show-info=true
check:
	docker exec -it educacode-cli-extract-information bash -c "./vendor/bin/pint -v"
	docker exec -it educacode-cli-extract-information bash -c "./vendor/bin/psalm --show-info=true"
coverage:
	docker exec -it -e XDEBUG_MODE=coverage educacode-cli-extract-information bash -c "./vendor/bin/phpunit --testsuite=Unit --coverage-html=var/cache/coverage"
build:
	docker build -t registry.gitlab.com/educa-code-labs/dev/back-end/projects/educacode-cli-php:base . --build-arg VERSION=1.0.0
push:
	docker push registry.gitlab.com/educa-code-labs/dev/back-end/projects/educacode-cli-php:base
