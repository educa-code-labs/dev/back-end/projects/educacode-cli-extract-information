<?php

namespace App\Forms;

use Rakit\Validation\Validation;
use Rakit\Validation\Validator;

class FormBase
{
    public Validator $validator;

    public function __construct()
    {
        $this->validator = new Validator();
    }

    public function rules(): array
    {
        return [];
    }

    public static function make(array $inputs): Validation
    {
        $obj = new self();

        return $obj->validator->make(inputs: $inputs, rules: $obj->rules());
    }
}
