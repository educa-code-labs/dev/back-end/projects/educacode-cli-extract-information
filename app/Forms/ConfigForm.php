<?php

namespace App\Forms;

class ConfigForm extends FormBase
{
    public function rules(): array
    {
        return [
            'version' => ['required', 'integer'],
            'config' => ['required', 'array'],
            'config.language' => ['required', 'in:PHP,JAVA'],
            'config.slug' => ['required', 'max:20'],
            'config.blurb' => ['required', 'min:5', 'max:100'],

            'exercises' => ['required', 'array'],

            'exercises.practice' => ['required', 'array'],
            'exercises.practice.*.slug' => ['required', 'min:1', 'max:40'],
            'exercises.practice.*.name' => ['required', 'min:5', 'max:20'],
            'exercises.practice.*.difficulty' => ['required', 'integer'],
        ];
    }
}
