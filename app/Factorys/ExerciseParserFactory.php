<?php

namespace App\Factorys;

use App\Dto\ConfigDto;
use App\Services\ExerciseParser;
use App\Services\PhpParser;

class ExerciseParserFactory
{
    private const PHP_LANGUAGE = 'PHP';

    public static function build(ConfigDto $dto, string $path): ExerciseParser
    {
        switch ($dto->getProject()->getLanguage()) {
            case self::PHP_LANGUAGE:

                return new PhpParser($dto, $path);

            default:

                throw new \Exception('Unable to build the interpreter you are looking for');
        }
    }
}
