<?php

namespace App\Exceptions;

use Throwable;

class MetaConfigException extends \Exception
{
    private array $erros = [];

    public function __construct(string $message, array $erros, int $code = 0, ?Throwable $previous = null)
    {
        $this->erros = $erros;
        parent::__construct($message, $code, $previous);
    }

    public function getErros(): array
    {
        return $this->erros;
    }
}
