<?php

namespace App\Traits;

use App\Helpers\DirectoryHelper;
use Exception;
use Illuminate\Support\Facades\File;

trait FileTrait
{
    /**
     * Method responsible for returning the contents of the configuration file.
     *
     * @throws Exception
     */
    public function getConfigContent(string $config_path): array
    {
        $config_content = File::get($config_path);
        $config_parse = yaml_parse($config_content);
        if (! is_array($config_parse)) {
            throw new Exception('Invalid config file');
        }

        return $config_parse;
    }

    /**
     * Method responsible for getting the configuration file path.
     */
    public function getConfigFilePath(): string
    {
        $dir_path = $this->option('dir');
        $dir_path = is_null($dir_path) || is_bool($dir_path) ? '.' : $dir_path;
        $dir_path = is_array($dir_path) ? (string) $dir_path[0] : $dir_path;

        $dir_path = DirectoryHelper::getDirectoryByCWD($dir_path);
        if (! $dir_path) {
            throw new Exception('The directory you are using is not valid');
        }

        $file_paht = "$dir_path/exercises-config.yml";
        $file_paht = str_replace('//', '/', $file_paht);

        if (! File::isFile($file_paht)) {
            throw new Exception('Could not find a configuration file in the directory');
        }

        return $file_paht;
    }
}
