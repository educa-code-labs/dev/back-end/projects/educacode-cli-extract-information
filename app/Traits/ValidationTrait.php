<?php

namespace App\Traits;

use App\Forms\FormBase;
use Rakit\Validation\Validator;

trait ValidationTrait
{
    public function validate(array $inputs, string $class): void
    {
        if (! class_exists($class)) {
            throw new \Exception("The validator $class you are looking for does not exist");
        }

        /** @var FormBase $obj */
        $obj = new $class();
        $validator = new Validator();
        $validation = $validator->validate($inputs, $obj->rules());

        if ($validation->fails()) {
            $this->error('There was an inconsistency in the validation of the information');

            $errors = $validation->errors();

            $table_data = [];
            foreach ($errors->toArray() as $key => $err) {
                foreach ($err as $e) {
                    $table_data[] = [$key, $e];
                }
            }

            $this->table(['Field', 'Message'], $table_data);
            abort(1, 'Please correct the information in the config file!!!');
        }
    }
}
