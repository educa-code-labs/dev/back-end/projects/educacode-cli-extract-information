<?php

namespace App\Dto;

class ConfigDto
{
    private string $version;

    private ProjectConfigDto $project;

    /**
     * @var ExercisePracticeDto[]
     */
    private array $practices;

    /**
     * @param  ExercisePracticeDto[]  $practices
     */
    public function __construct(string $version, ProjectConfigDto $project, array $practices)
    {
        $this->version = $version;
        $this->project = $project;
        $this->practices = $practices;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function getProject(): ProjectConfigDto
    {
        return $this->project;
    }

    public function getPractices(): array
    {
        return $this->practices;
    }

    public function toArray(): array
    {
        return [
            'version' => $this->getVersion(),
            'project' => $this->getProject()->toArray(),
            'practices' => array_map(fn (ExercisePracticeDto $practiceDto) => $practiceDto->toArray(), $this->getPractices()),
        ];
    }
}
