<?php

namespace App\Dto;

class ProjectConfigDto
{
    private string $language;

    private string $slug;

    private string $blurb;

    private array $tags;

    public function __construct(string $language, string $slug, string $blurb, array $tags = [])
    {
        $this->language = $language;
        $this->slug = $slug;
        $this->blurb = $blurb;
        $this->tags = $tags;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getBlurb(): string
    {
        return $this->blurb;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function toArray(): array
    {
        return [
            'language' => $this->getLanguage(),
            'slug' => $this->getSlug(),
            'blurb' => $this->getBlurb(),
            'tags' => $this->getTags(),
        ];
    }
}
