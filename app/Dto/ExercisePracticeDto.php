<?php

namespace App\Dto;

class ExercisePracticeDto
{
    private string $slug;

    private string $name;

    private int $difficulty;

    private array $topics;

    private array $data;

    public function __construct(string $slug, string $name, int $difficulty, array $topics = [], array $data = [])
    {
        $this->slug = $slug;
        $this->name = $name;
        $this->difficulty = $difficulty;
        $this->topics = $topics;
        $this->data = $data;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    public function getTopics(): array
    {
        return $this->topics;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function toArray(): array
    {
        return [
            'slug' => $this->getSlug(),
            'name' => $this->getName(),
            'difficulty' => $this->getDifficulty(),
            'topics' => $this->getTopics(),
            'data' => $this->getData(),
        ];
    }
}
