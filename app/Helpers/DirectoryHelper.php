<?php

namespace App\Helpers;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class DirectoryHelper
{
    public static function getDirectoryByCWD(?string $dir): string|false
    {
        if (! $dir || $dir == '.') {
            return getcwd();
        }

        if (is_dir($dir)) {
            return $dir;
        }

        $dir_full = getcwd()."/$dir";
        $dir_full = str_replace('//', '/', $dir_full);

        if (is_dir($dir_full)) {
            return $dir_full;
        }

        return false;
    }

    public static function getAllFilesInDir(string $dir): array
    {
        $rii = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir)
        );

        /** @var string[] $files */
        $files = [];
        foreach ($rii as $file) {
            if ($file->isDir()) {
                continue;
            }

            $files[] = $file->getPathname();
        }

        return $files;
    }

    public static function generateHashFromDir(string $dir): string
    {
        $files = self::getAllFilesInDir($dir);
        $hash = '';

        foreach ($files as $file) {
            $hash .= sprintf('%s.', md5_file($file));
        }

        return md5($hash);
    }
}
