<?php

namespace App\Helpers;

use App\Dto\ConfigDto;
use App\Dto\ExercisePracticeDto;
use App\Dto\ProjectConfigDto;

class DtoHelper
{
    public static function parseConfigDto(array $data): ConfigDto
    {
        $practices = [];
        foreach ($data['exercises']['practice'] as $item) {
            $practices[] = new ExercisePracticeDto(
                slug: $item['slug'],
                name: $item['name'],
                difficulty: $item['difficulty'],
                topics: $item['topics'] ?? []
            );
        }

        return new ConfigDto(
            version: $data['version'],
            project: new ProjectConfigDto(
                language: $data['config']['language'],
                slug: $data['config']['slug'],
                blurb: $data['config']['blurb'],
                tags: $data['config']['tags'] ?? []
            ),
            practices: $practices,
        );
    }
}
