<?php

namespace App\Commands;

use App\Dto\ConfigDto;
use App\Factorys\ExerciseParserFactory;
use App\Helpers\DtoHelper;
use App\Traits\FileTrait;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;

class PushCommand extends Command
{
    use FileTrait;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'push {--D|dir= : Directory where the project is stored} {--disable-http : Disable HTTP call to API (optional)}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'The push command is used to push the contents of the local repository to a remote repository.';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->runValidateConfig();
        $this->runCheckEnv();

        $config_path = $this->getConfigFilePath();
        $dir_path = Str::replace('exercises-config.yml', '', $config_path);
        $config_parse = $this->getConfigContent($config_path);

        $dto = DtoHelper::parseConfigDto($config_parse);
        $dto_array = $this->getOutputArray($dto, $dir_path);

        $this->runSaveOutputFile($dto_array, $dir_path);

        if (! $this->option('disable-http')) {
            $this->runPushData($dto_array);
        }
    }

    /**
     * Method responsible for calling the validation command.
     */
    public function runValidateConfig(): void
    {
        $this->task('Validate Config', function () {
            $this->newLine();
            $this->call('validate', [
                '--dir' => $this->option('dir'),
            ]);

            return true;
        });
    }

    /**
     * Method responsible for checking the environment variables.
     */
    public function runCheckEnv(): void
    {
        $this->task('Checking remote url', function () {
            return ! empty(env('EDUCACODE_REMOTE_URL'));
        });

        $this->task('Checking token', function () {
            return ! empty(env('EDUCACODE_REMOTE_TOKEN'));
        });
    }

    /**
     * Method responsible for getting the processed information.
     *
     * @return array|false[]
     *
     * @throws \Exception
     */
    public function getOutputArray(ConfigDto $dto, string $dir_path): array
    {
        $exercises = ExerciseParserFactory::build($dto, $dir_path)->getExercises();
        foreach ($dto->getPractices() as $practice) {
            foreach ($exercises as $exercise) {
                if ($exercise['info']['slug'] == $practice->getSlug()) {
                    $practice->setData($exercise);

                    $this->task('Processing '.$practice->getName(), function () {
                        return true;
                    });
                }
            }
        }

        $dto_array = ['hash' => false];
        $dto_array = array_merge($dto_array, $dto->toArray());
        $dto_array['hash'] = md5(json_encode($dto_array));

        return $dto_array;
    }

    /**
     * Method responsible for saving the processed information in a JSON file.
     */
    public function runSaveOutputFile(array $dto_array, string $dir_path): void
    {
        $this->task('Save file output', function () use ($dto_array, $dir_path) {
            $cache_dir = "$dir_path/.cache/educacode-cli";
            if (! File::exists($cache_dir)) {
                File::makeDirectory(path: $cache_dir, recursive: true);
            }

            $data = json_encode($dto_array, JSON_PRETTY_PRINT);
            File::put("$cache_dir/exercises-output.json", $data);

            return true;
        });
    }

    /**
     * Method responsible for sending the processed information to the server.
     */
    public function runPushData(array $dto_array): void
    {
        $this->task('Push data', function () use ($dto_array) {
            $url = env('EDUCACODE_REMOTE_URL');
            $token = env('EDUCACODE_REMOTE_TOKEN');

            try {
                $response = Http::withToken($token)->post($url, $dto_array);

                return $response->ok();
            } catch (ConnectionException $exception) {
                $this->newLine();
                $this->error($exception->getMessage());

                return false;
            }
        });
    }
}
