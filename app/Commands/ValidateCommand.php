<?php

namespace App\Commands;

use App\Exceptions\MetaConfigException;
use App\Factorys\ExerciseParserFactory;
use App\Forms\ConfigForm;
use App\Helpers\DtoHelper;
use App\Traits\FileTrait;
use App\Traits\ValidationTrait;
use Exception;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;

class ValidateCommand extends Command
{
    use ValidationTrait;
    use FileTrait;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'validate {--dir= : Directory where the project is stored}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'To verify that the configuration is valid';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        try {
            $config_path = $this->getConfigFilePath();
            $dir_path = Str::replace('exercises-config.yml', '', $config_path);

            $config_parse = $this->getConfigContent($config_path);

            $this->validateConfig($config_parse);

            $dto = DtoHelper::parseConfigDto($config_parse);
            ExerciseParserFactory::build($dto, $dir_path)->validateExercises();

            $this->info('Verification was successful');
        } catch (MetaConfigException $exception) {
            $this->warn($exception->getMessage());
            $this->warn('See the list of errors below:');
            $this->newLine(2);

            foreach ($exception->getErros() as $key => $erro) {
                $this->warn("$key - $erro");
            }

            abort(1, 'Please make the necessary correction in the file');
        } catch (Exception $exception) {
            abort(1, $exception->getMessage());
        }
    }

    /**
     * Method responsible for initiating configuration file validation.
     */
    public function validateConfig(array $content): void
    {
        $version = $content['version'] ?? '1';

        try {
            $reponse = match ($version) {
                '1' => function () use ($content) {
                    $this->validate($content, ConfigForm::class);
                },
                default => function () {
                    throw new Exception('The version your config file is defending is invalid.');
                },
            };

            $reponse();
        } catch (Exception $exception) {
            abort(1, $exception->getMessage());
        }
    }
}
