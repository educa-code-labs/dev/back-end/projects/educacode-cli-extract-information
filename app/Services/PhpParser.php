<?php

namespace App\Services;

use App\Dto\ConfigDto;
use App\Exceptions\MetaConfigException;
use App\Forms\ExerciseForm;
use App\Helpers\DirectoryHelper;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Rakit\Validation\Validator;

class PhpParser implements ExerciseParser
{
    public function __construct(private readonly ConfigDto $dto, private readonly string $path)
    {

    }

    /**
     * Method responsible for reading the exercise configuration file
     *
     * @throws \Exception
     */
    public function getExerciseConfig(string $exercise_path): array
    {
        if (File::exists($exercise_path)) {
            return File::json($exercise_path);
        }

        throw new \Exception('Configuration file not found in project');
    }

    /**
     * Method responsible for reading the files related to the exercise.
     */
    public function getExerciseFiles(string $exercise_path, array $files): array
    {
        $data = [];

        foreach (['solution', 'test', 'example'] as $type) {
            foreach ($files[$type] as $file) {
                $data[$type.'_files'][] = [
                    'name' => $file,
                    'raw' => File::get("$exercise_path/$file"),
                ];
            }
        }

        return $data;
    }

    /**
     * Method responsible for reading the main exercise documentation files.
     */
    public function getExerciseDocs(string $exercise_path, array $config): array
    {
        $docs = [];
        $docs[$config['default_language']] = [
            'help' => File::get("$exercise_path/HELP.md"),
            'readme' => File::get("$exercise_path/README.md"),
        ];

        foreach ($config['translations'] as $translation) {
            $docs[$translation] = [
                'help' => File::get("$exercise_path/.docs/lang/HELP-$translation.md"),
                'readme' => File::get("$exercise_path/.docs/lang/README-$translation.md"),
            ];
        }

        return $docs;
    }

    /**
     * Method responsible for reading the exercises from the Dto configuration
     */
    public function getExercises(): array
    {
        $exercises_path = sprintf('%s/exercises/', $this->path);
        $exercises_dir = array_diff(scandir($exercises_path), ['.', '..']);
        $data = [];

        foreach ($exercises_dir as $exercise) {
            foreach ($this->dto->getPractices() as $practice) {
                if ($practice->getSlug() == Str::kebab($exercise)) {
                    $path = Str::replace(search: '//', replace: '/', subject: "$exercises_path/$exercise");
                    $data[] = $this->parseExercise($path, $exercise);
                }
            }
        }

        return $data;
    }

    public function validateExercises(): void
    {
        $exercises_path = sprintf('%s/exercises/', $this->path);
        if (! File::exists($exercises_path)) {
            throw new FileNotFoundException('No valid file structure found for PHP project');
        }

        $exercises_dir = array_diff(scandir($exercises_path), ['.', '..']);
        foreach ($this->dto->getPractices() as $practice) {
            $has_find = false;

            foreach ($exercises_dir as $exercise) {
                if ($practice->getSlug() == Str::kebab($exercise)) {
                    $path = Str::replace(search: '//', replace: '/', subject: "$exercises_path/$exercise");
                    $meta_path = "$path/.meta/config.json";

                    if (! File::exists($meta_path)) {
                        throw new FileNotFoundException("The configuration file ($meta_path) for the exercise was not found");
                    }

                    $this->validateMetaConfig($meta_path);

                    try {
                        $this->parseExercise($path, $exercise);
                        $has_find = true;

                    } catch (\Exception $exception) {
                        throw new \Exception('A validation error occurred: '.$exception->getMessage());
                    }
                }
            }

            if (! $has_find) {
                throw new FileNotFoundException('Unable to load configuration files for configuration: '.$practice->getSlug());
            }
        }
    }

    /**
     * Method responsible for bringing the language used in parse.
     */
    public function getLanguage(): string
    {
        return $this->dto->getProject()->getLanguage();
    }

    /**
     * Method responsible for collecting exercise information
     *
     * @throws \Exception
     */
    public function parseExercise(string $path, string $exercise_name): array
    {
        $config = $this->getExerciseConfig("$path/.meta/config.json");
        $files = $this->getExerciseFiles($path, $config['files']);
        $docs = $this->getExerciseDocs($path, $config);

        return [
            'info' => [
                'path' => $path,
                'slug' => Str::kebab($exercise_name),
                'name' => $exercise_name,
                'hash' => DirectoryHelper::generateHashFromDir($path),
            ],
            'config' => $config,
            'files' => $files,
            'docs' => $docs,
        ];
    }

    /**
     * @throws \Exception
     */
    public function validateMetaConfig(string $meta_path): void
    {
        $meta_config = File::json($meta_path);
        $obj = new ExerciseForm();
        $validator = new Validator();
        $validation = $validator->validate($meta_config, $obj->rules());

        if ($validation->fails()) {
            throw new MetaConfigException(
                message: 'A validation error occurred in the exercise configuration file',
                erros: $validation->errors()->all(),
            );
        }
    }
}
