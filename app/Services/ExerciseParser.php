<?php

namespace App\Services;

interface ExerciseParser
{
    public function getExercises(): array;

    public function validateExercises(): void;

    public function getLanguage(): string;
}
